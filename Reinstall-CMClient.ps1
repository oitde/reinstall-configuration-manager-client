#Reinstalls the CM (SCCM) client when troubleshooting client issues is needed.
#Run Powershell as administrator before starting if not deploying through an EPM tool. 

#Run this step first by clicking on line 5 and pressing F8. Then run the full script by pressing F5.
#Set-ExecutionPolicy Unrestricted -Scope Process -Force

$CCMDir="C:\Windows\CCM"
$CCM_Temp= Join-Path -Path $CCMDir -ChildPath "ClientTemp"
$CMSetup= "C:\Windows\ccmsetup"

#Create temp directory for local CM client installer before uninstalling
New-Item -ItemType Directory -Path $CCM_Temp -Force | Out-Null

#Copy ccmsetup folder to temp directory before uninstalling so it's not deleted
Copy-Item -Path "$CMSetup\*" -Destination $CCM_Temp -Recurse -Force

#Stop CM client service
Stop-Service -Name "ccmexec" -Force

#Uninstall CM client
Write-Host "Uninstalling CM client ........."
$uninstallCommand = "C:\Windows\ccmsetup\ccmsetup.exe /uninstall"
Start-Process -FilePath "cmd.exe" -ArgumentList "/c", $uninstallCommand -Wait -WindowStyle Hidden

#Copy ccmsetup files from temp directory back to source directory before installing
Copy-Item -Path "$CCM_Temp\*" -Destination "$CMSetup" -Recurse -Force

#Install CM client
Write-Host "Installing CM client ........."
$installCommand = "C:\Windows\ccmsetup\ccmsetup.exe"
Start-Process -FilePath "cmd.exe" -ArgumentList "/c", $installCommand -Wait -WindowStyle Hidden

# Restart CM client service
Write-Host "Restarting CM client service ........."
Start-Service -Name "ccmexec"
